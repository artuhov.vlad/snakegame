// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "BonusExtraSpeed.h"
#include "Wall.h"
#include <random>
#include <ctime>
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	FoodCounter = 0;
	PlayerScore = 0;
	EndGameMessage = "";

	for (int i = 0; i < ZONERANGE*2+1; ++i)
	{
		std::fill_n(GameZoneMatrix[i], ZONERANGE*2+1, 1);
	}
	InnerWallsAmount = 10;
	GameZoneCapacity = 0;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	srand(time(NULL));
	if (InnerWallsAmount > ZONERANGE * ZONERANGE * 4 + ZONERANGE * 2)
	{
		InnerWallsAmount = ZONERANGE * ZONERANGE * 4 + ZONERANGE * 2;
	}
	SpawnInnerWalls(InnerWallsAmount);
	SpawnFood();
	FoodCounter++;
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (FoodCounter < 2)
	{
		SpawnFood();
		FoodCounter++;
	}

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalPlayerInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalPlayerInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->PlayerPawn = this;
	SnakeActor->AddSnakeElement(3);
	SnakeActor->OnDestroyed.AddDynamic(this, &APlayerPawnBase::SnakeActorDestroyed);
}


FVector APlayerPawnBase::GenerateRandomCoord()
{
	
	int32 XCoord = rand() % (ZONERANGE * 2 + 1);
	int32 YCoord = rand() % (ZONERANGE * 2 + 1);
	int32 maxCapacity = ZONERANGE * ZONERANGE * 4 + ZONERANGE * 4 + 1;

	while (!IsSpawnCellFree(XCoord, YCoord) && (GameZoneCapacity < maxCapacity))
	{
		XCoord = rand() % (ZONERANGE * 2 + 1);
		YCoord = rand() % (ZONERANGE * 2 + 1);
	}


	XCoord -= ZONERANGE;
	YCoord -= ZONERANGE;
	XCoord *= CELLSCALE;
	YCoord *= CELLSCALE;
	
	return FVector(XCoord, YCoord, 0);
}

bool APlayerPawnBase::IsSpawnCellFree(int32 x, int32 y)
{
	if ((x >= 0 && x <= ZONERANGE * 2) && (y >= 0 && y <= ZONERANGE * 2))
	{
		if (GameZoneMatrix[x][y] == 1)
		{
			return true;
		}
		else return false;
	}
	else return false;
}

void APlayerPawnBase::FreeCell(FVector Location)
{
	int32 X = 0;
	int32 Y = 0;
	ReverseCellIndexes(Location, X, Y);

	if ((X >= 0 && X <= ZONERANGE * 2) && (Y >= 0 && Y <= ZONERANGE * 2))
	{
		GameZoneMatrix[X][Y] = 1;
		--GameZoneCapacity;
	}
}

void APlayerPawnBase::OccupyCell(FVector Location)
{
	int32 X = 0;
	int32 Y = 0;
	int32 maxCapacity = ZONERANGE * ZONERANGE * 4 + ZONERANGE * 4 + 1;
	ReverseCellIndexes(Location, X, Y);

	if ((X >= 0 && X <= ZONERANGE * 2) && (Y >= 0 && Y <= ZONERANGE * 2))
	{
		GameZoneMatrix[X][Y] = 0;
		++GameZoneCapacity;
		if (GameZoneCapacity == maxCapacity)
		{
			SnakeActor->Destroy();
		}
	}
}

void APlayerPawnBase::ReverseCellIndexes(FVector location, int32 &x, int32 &y)
{
	x = location.X / CELLSCALE;
	x += ZONERANGE;
	y = location.Y / CELLSCALE;
	y += ZONERANGE;
}

void APlayerPawnBase::HandleVerticalPlayerInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN && !SnakeActor->bIsMovingKeyPressed)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			SnakeActor->bIsMovingKeyPressed = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP && !SnakeActor->bIsMovingKeyPressed)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->bIsMovingKeyPressed = true;
		}
	}
}

void APlayerPawnBase::HandleHorizontalPlayerInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT && !SnakeActor->bIsMovingKeyPressed)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			SnakeActor->bIsMovingKeyPressed = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT && !SnakeActor->bIsMovingKeyPressed)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->bIsMovingKeyPressed = true;
		}
	}
}

void APlayerPawnBase::SpawnFood()
{
	
	FVector NewLocation = GenerateRandomCoord();	
	OccupyCell(NewLocation);
	AFood* FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform(NewLocation));
	FoodActor->OnDestroyed.AddDynamic(this, &APlayerPawnBase::FoodItemDestroyed);	
}

void APlayerPawnBase::SpawnBonus()
{
	FVector NewLocation = GenerateRandomCoord();
	OccupyCell(NewLocation);
	ABonusExtraSpeed* BonusActor = GetWorld()->SpawnActor<ABonusExtraSpeed>(BonusExtraSpeedClass, FTransform(NewLocation));
	BonusActor->OnDestroyed.AddDynamic(this, &APlayerPawnBase::BonusItemDestroyed);
}

void APlayerPawnBase::FoodItemDestroyed(AActor* Act)
{
	FVector Location = Act->GetActorLocation();	
	FreeCell(Location);
	FoodCounter--;
	AFood* Food = Cast<AFood>(Act);
	if (Food->bWasEaten)
	{
		PlayerScore += 10;
		if (rand() % 4 == 1)
		{
			SpawnBonus();
		}
	}
	
}

void APlayerPawnBase::BonusItemDestroyed(AActor* Act)
{
	FVector Location = Act->GetActorLocation();
	FreeCell(Location);
	ABonusExtraSpeed* Bonus = Cast<ABonusExtraSpeed>(Act);
	if (Bonus->bWasEaten)
	{
		PlayerScore += 50;
	}
	
}

void APlayerPawnBase::SnakeActorDestroyed(AActor* Act)
{
	int32 maxCapacity = ZONERANGE * ZONERANGE * 4 + ZONERANGE * 4 + 1;
	if (GameZoneCapacity == maxCapacity)
		EndGameMessage = "WIN!!!";
	else 
		EndGameMessage = "GAME OVER";
}

void APlayerPawnBase::InnerWallDestroyed(AActor* Act)
{
	FVector Location = Act->GetActorLocation();
	FreeCell(Location);
	SpawnInnerWalls(1);
}

void APlayerPawnBase::SpawnInnerWalls(int32 WallsNum /*= 4*/)
{

	int x(0), y(0);
	FVector NewLocation;
	for (int i = 0; i < WallsNum; ++i)
	{
		do 
		{
			NewLocation = GenerateRandomCoord();
			ReverseCellIndexes(NewLocation, x, y);
		} while (y == 14);
		
		NewLocation.Z = -40;
		OccupyCell(NewLocation);
		AWall* WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, FTransform(NewLocation));
		WallActor->OnDestroyed.AddDynamic(this, &APlayerPawnBase::InnerWallDestroyed);
	}
	
}

