// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

#define ZONERANGE 14
#define CELLSCALE 60

class UCameraComponent;
class ASnakeBase;
class AFood;
class ABonusExtraSpeed;
class AWall;


UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWall> WallActorClass;

	UPROPERTY(BlueprintReadWrite)
	int32 FoodCounter;

	UPROPERTY(BlueprintReadWrite)
	int32 PlayerScore;

	UPROPERTY(BlueprintReadWrite)
	FString EndGameMessage;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonusExtraSpeed> BonusExtraSpeedClass;

	UPROPERTY(EditDefaultsOnly)
	int32 InnerWallsAmount;

	int32 GameZoneCapacity;
	int32 GameZoneMatrix[ZONERANGE * 2 + 1][ZONERANGE * 2 + 1];

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void	CreateSnakeActor();
	FVector GenerateRandomCoord();
	bool	IsSpawnCellFree(int32 x, int32 y);
	void	FreeCell(FVector Location);
	void    OccupyCell(FVector Location);
	void	ReverseCellIndexes(FVector location, int32& x, int32& y);

	UFUNCTION()
	void HandleVerticalPlayerInput(float value);
	UFUNCTION()
	void HandleHorizontalPlayerInput(float value);
	UFUNCTION()
	void SpawnFood();
	UFUNCTION()
	void SpawnBonus();
	UFUNCTION()
	void FoodItemDestroyed(AActor* Act);
	UFUNCTION()
	void BonusItemDestroyed(AActor* Act);
	UFUNCTION()
	void SnakeActorDestroyed(AActor* Act);
	UFUNCTION()
	void InnerWallDestroyed(AActor* Act);
	UFUNCTION()
	void SpawnInnerWalls(int32 WallsNum = 4);

};
