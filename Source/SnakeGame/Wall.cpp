// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"


// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bIsHorizontalTeleport = false;
	bIsVerticalTeleport = false;
	LifeTimer = 100 + (rand() % 40);
	this->SetActorTickInterval(0.5);
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!this->bIsHorizontalTeleport && !this->bIsVerticalTeleport)
	{
		if (LifeTimer > 0)
		{
			LifeTimer -= 0.5;
		}
		else
			this->Destroy();
	}
	

}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (bIsHorizontalTeleport)
			{
				ASnakeElementBase* Head = Snake->SnakeElements[0];
				FVector Location = Head->GetActorLocation();
				FVector NewLocation;
				if (Snake->LastMoveDirection == EMovementDirection::DOWN)
				{
					NewLocation = FVector(-(Location.X + Snake->ElementSize), Location.Y, 0);
				}
				else if (Snake->LastMoveDirection == EMovementDirection::UP)
				{
					NewLocation = FVector(-(Location.X - Snake->ElementSize), Location.Y, 0);
				}

				Head->SetActorLocation(NewLocation);
			} 
			else if (bIsVerticalTeleport)
			{
				ASnakeElementBase* Head = Snake->SnakeElements[0];
				FVector Location = Head->GetActorLocation();
				FVector NewLocation;
				if (Snake->LastMoveDirection == EMovementDirection::RIGHT)
				{
					NewLocation = FVector(Location.X, -(Location.Y + Snake->ElementSize), 0);
				}
				else if (Snake->LastMoveDirection == EMovementDirection::LEFT)
				{
					NewLocation = FVector(Location.X, -(Location.Y - Snake->ElementSize), 0);
				}
				
				Head->SetActorLocation(NewLocation);
			}
			else
			{
				Snake->Destroy();
			}


		}
	}
}

