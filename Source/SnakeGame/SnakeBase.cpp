// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementSpeed = 0.5f;
	PlayerPawn = nullptr;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	bIsMovingKeyPressed = false;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	if (bIsMovingKeyPressed)
	{
		bIsMovingKeyPressed = false;
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocaion = FVector(SnakeElements.Num() * ElementSize, 0, 0);
		if (ElementsNum == 1 && SnakeElements.Num() > 0)
		{
			NewLocaion = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}

		PlayerPawn->OccupyCell(NewLocaion);
		FTransform NewTransform(NewLocaion);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
	
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector =  FVector(0,0,0);
	float SnakeMovementSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += SnakeMovementSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= SnakeMovementSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += SnakeMovementSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= SnakeMovementSpeed;
		break;
	default:
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	PlayerPawn->FreeCell(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation());

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	PlayerPawn->OccupyCell(SnakeElements[0]->GetActorLocation());
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* IInteractableInterface = Cast<IInteractable>(Other);
		if (IInteractableInterface)
		{
			IInteractableInterface->Interact(this, bIsFirst);
		}
	}
}

